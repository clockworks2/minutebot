package com.clockworks;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.ChunkingFilter;
import net.dv8tion.jda.api.utils.Compression;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

public class Discord {
    private static final Logger logger = LoggerFactory.getLogger(Discord.class);
    private static final String DATA_FILE = ".data/discordauth.json"; 

    private final Executor messageWaiter = Executors.newFixedThreadPool(4);

    private final JDA jda;
    private final Guild guild;
    private final TextChannel debugChannel;

    public Discord() {
        String guildId, debugChannelId;
        try {
            String json = Files.readString(Paths.get(DATA_FILE));
            JSONObject jsonObj = new JSONObject(json);
            String token = jsonObj.getString("token");
            guildId = jsonObj.getString("guildId");
            debugChannelId = jsonObj.getString("debugChannelId");
            JDABuilder jdaBuilder = JDABuilder.createDefault(token)
                .setCompression(Compression.ZLIB)
                .setActivity(Activity.watching("your minutes"));
            jda = jdaBuilder.build();
            Thread.sleep(5000);
        } catch (Exception e) {
            logger.error("Cannot load discord bot", e);
            throw new RuntimeException(e);
        }
        guild = jda.getGuildById(guildId);
        if (guild == null) {
            logger.error("Could not find guild");
            for (Guild g : jda.getGuilds()) {
                logger.error(g.getName() + " | " + g.getId());
            }
            throw new RuntimeException();
        }
        debugChannel = guild.getTextChannelById(debugChannelId);
        if (debugChannel == null) {
            logger.error("Could not find debug channel");
            throw new RuntimeException();
        }

        StringBuilder sb = new StringBuilder();
        guild.getChannels().stream().filter(c -> c.getType().equals(ChannelType.VOICE)).forEach(c -> {
            sb.append(c.getName()).append(" -> ").append(c.getMembers().size()).append("\n");
        });

        if (guild.getSelfMember().hasPermission(debugChannel, Permission.MESSAGE_WRITE)) {
            debugChannel.sendMessage(sb.toString()).complete();
        } else logger.error("No write permission for channel");
        logger.info(sb.toString());

    }


    
}