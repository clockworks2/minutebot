package com.clockworks.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Utils {
    public static String generateFortune() {
        try {
            String out = "";
            Process proc = Runtime.getRuntime().exec("fortune -s");
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String s = null;
            while ((s = stdInput.readLine()) != null) {
                out = out + "\n" + s;
            }
            return out.substring(1);
        } catch (IOException ioex) {
            ioex.printStackTrace();
        }
        return "";
    }
}