package com.clockworks.schedule;

import org.json.JSONObject;

public interface JSONPersistent {
    JSONObject serialize();
    void deserialize(JSONObject instance);
    
}