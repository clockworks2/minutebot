package com.clockworks.schedule;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.StreamSupport;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class Multijob implements JSONPersistent {
    private final PriorityQueue<Timer> timerQueue = new PriorityQueue<>(Comparator.comparing(t -> t.getCallTime()));   
    private final Executor executor = Executors.newSingleThreadExecutor();
    private boolean initialized = false;

    boolean tryInitialize(JSONObject serialized) {
        if (serialized != null) {
            if (serialized.has("versionId") && compatibleWithVersionId(serialized.getString("versionId")) &&
                serialized.has("name") && getName().equals(serialized.getString("name"))) {
                this.deserialize(serialized);
                initialized = true;
            }
        } else {
            init();
            initialized = true;
        }
        return initialized;
    }

    public final boolean isInitialized() {
        return initialized;
    }

    protected final void unregisterTimer(String id) {
        timerQueue.removeIf(t -> t.getId().equals(id));
    }

    protected final void registerTimer(LocalDateTime when, String id) {
        if (timerQueue.stream().filter(t -> t.getId().equals(id)).findAny().isPresent()) {
            timerQueue.removeIf(t -> t.getId().equals(id));
        }
        timerQueue.add(new Timer(id, when));
    }

    void onRawTimer(LocalDateTime now) {
        executor.execute(() -> {
            while (!timerQueue.isEmpty()) {
                Timer current = timerQueue.poll();
                int compare = current.getCallTime().compareTo(now);
                if (compare <= 0) {
                    onTimer(current.getId(), current.getCallTime());
                } else {
                    timerQueue.offer(current);
                    break;
                }
            }
        });
    }

    public abstract String getVersionId();

    protected abstract boolean compatibleWithVersionId(String versionId);

    public abstract String getName();

    protected abstract void init();

    protected abstract void onTimer(String id, LocalDateTime scheduled);

    @Override
    public void deserialize(JSONObject instance) {
        timerQueue.clear();
        StreamSupport.stream(instance.getJSONArray("timers").spliterator(), false)
            .map(o -> (JSONObject) o)
            .forEach(o -> timerQueue.offer(new Timer(o)));
    }

    @Override
    public JSONObject serialize() {
        JSONObject cont = new JSONObject();
        cont.put("versionId", getVersionId());
        cont.put("name", getName());
        JSONArray jsonTimers = new JSONArray();
        timerQueue.stream().map(t -> t.serialize()).forEach(o -> {
            jsonTimers.put(o);
        });
        cont.put("timers", jsonTimers);
        return cont;
    }

    private class Timer {
        private final String id;
        private final LocalDateTime callTime;

        private Timer(String id, LocalDateTime callTime) {
            this.id = id;
            this.callTime = callTime;
        }
        
        private Timer(JSONObject json) {
            this.id = json.getString("id");
            this.callTime = LocalDateTime.parse(json.getString("callTime"));
        }

        public String getId() {
            return id;
        }

        public LocalDateTime getCallTime() {
            return callTime;
        }

        private JSONObject serialize() {
            return new JSONObject()
                .put("id", id)
                .put("callTime", callTime.toString());
        }
    }
}