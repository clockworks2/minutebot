package com.clockworks.schedule;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.clockworks.Drive;
import com.clockworks.Slack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JobScheduler {
    private static final Logger logger = LoggerFactory.getLogger(JobScheduler.class);

    private final Scheduler scheduler;
    private final List<Multijob> jobs;
    private final List<JSONObject> savedJobs;

    public JobScheduler() {
        try {
            scheduler = StdSchedulerFactory.getDefaultScheduler();
        } catch (SchedulerException schex) {
            throw new RuntimeException("Failed to initialize scheduler", schex);
        }
        jobs = new ArrayList<>();
        savedJobs = new ArrayList<>();
    }

    public void initializeAsSavedInstance(String jsonString) {
        JSONArray array;
        try {
            array = new JSONArray(jsonString);
        } catch (JSONException exc) {
            logger.error("Cannot load saved jobs array", exc);
            return;
        }
        array.forEach(o -> {
            try {
                savedJobs.add((JSONObject) o);
            } catch (ClassCastException exc) {
                logger.error("Cannot load saved job", exc);
            }
        });
    }

    public void addMultijob(Multijob job) {
        jobs.add(job);
        for (var iterator = savedJobs.iterator(); iterator.hasNext(); ) {
            if (job.tryInitialize(iterator.next())) {
                iterator.remove();
                break;
            }
        }
        if (!job.isInitialized()) {
            job.tryInitialize(null);
        }
    }

    public void start() {
        try {
            scheduler.start();
            JobDataMap jdm = new JobDataMap();
            jdm.put("instance", this);
            JobDetail jd = JobBuilder.newJob(JobExecutor.class)
                .withIdentity("periodic_job", "root")
                .usingJobData(jdm)
                .build();
            Trigger trg = TriggerBuilder.newTrigger()
                .withIdentity("periodic_trigger", "root")
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(1).withMisfireHandlingInstructionNextWithExistingCount())
                .startNow()
                .build();
            scheduler.scheduleJob(jd, trg);
        } catch (SchedulerException schex) {
            logger.error("Cannot start cron", schex);
        }
    }

    public String stop() {
        try {
            scheduler.shutdown(true);
        } catch (SchedulerException schex) {
            logger.error("Could not shut down quartz scheduler", schex);
        }
        JSONArray array = new JSONArray();
        for (var job : jobs) {
            array.put(job.serialize());
        }
        for (var job : savedJobs) {
            array.put(job);
        }
        return array.toString(2);
    }

    private void onPeriodicTimer(LocalDateTime now) {
        for (Multijob job : jobs) {
            job.onRawTimer(now);
        }
    }

    public static class JobExecutor implements Job {
        @Override
        public void execute(JobExecutionContext context) throws JobExecutionException {
            JobScheduler jobsch = (JobScheduler) context.getMergedJobDataMap().get("instance");
            jobsch.onPeriodicTimer(Instant.ofEpochMilli(context.getFireTime().getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime());
        }
    }
}