package com.clockworks;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;

public class Drive {
    private static final String APPLICATION_NAME = "MinuteBot";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = ".data/tokens";
    private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);
    private static final String CREDENTIALS_FILE_PATH = ".data/credentials.json";

    public static final String FIELDS = "id, name, mimeType, modifiedTime, parents";
    public static final String QUERY_FIELDS = "nextPageToken, files(" + FIELDS + ")";

    com.google.api.services.drive.Drive service;

    

    public Drive() {
        try {
            NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            service = new com.google.api.services.drive.Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public File getFile(String id) {
        try {
            File file = service.files().get(id)
                .setFields(FIELDS)
                .execute();
            return file;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String getViewLink(String id, boolean createNewPermission) {
        try {
            if (createNewPermission) {
                Permission shareable = new Permission()
                .setType("anyone")
                .setRole("reader");
            service.permissions().create(id, shareable)
                .setFields("id")
                .execute();
            }
            File file = service.files().get(id)
                .setFields("webViewLink")
                .execute();
            return file.getWebViewLink();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    public List<File> listFolder(String id) {
        try {
            List<File> files = new ArrayList<>();
            String pager = null;
            do {
                FileList list = service.files().list()
                    .setQ(String.format("\'%s\' in parents and trashed=false", id))
                    .setSpaces("drive")
                    .setFields(QUERY_FIELDS)
                    .setPageSize(50)
                    .setPageToken(pager)
                    .execute();
                pager = list.getNextPageToken();
                files.addAll(list.getFiles());
            } while (pager != null);
            return files;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public File moveFile(String id, String newLocationId) {
        File subject = getFile(id);
        if (subject != null) {
            String parentToRemove = subject.getParents().stream().collect(Collectors.joining(","));
            try {
                return service.files().update(id, null)
                    .setAddParents(newLocationId)
                    .setRemoveParents(parentToRemove)
                    .setFields(FIELDS)
                    .execute();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    public File copyFile(String id, String newName) {
        File newFile = new File();
        newFile.setName(newName);
        try {
            String copyId = service.files().copy(id, newFile).execute().getId();
            return getFile(copyId);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public boolean isFolder(String id) {
        File f = getFile(id);
        if (f == null) throw new RuntimeException("No such file");
        return f.getMimeType().equals("application/vnd.google-apps.folder");
    }



    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        InputStream in = Files.newInputStream(Paths.get(CREDENTIALS_FILE_PATH));
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }
}