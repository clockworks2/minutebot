package com.clockworks.job;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.stream.Stream;

import com.clockworks.Discord;
import com.clockworks.Drive;
import com.clockworks.Slack;
import com.clockworks.schedule.Multijob;
import com.clockworks.utils.Utils;
import com.google.api.services.drive.model.File;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MinuteJob extends Multijob {
    private static final Logger logger = LoggerFactory.getLogger(MinuteJob.class);
    private static final DayOfWeek meetingDay = DayOfWeek.SATURDAY;
    private static final LocalTime meetingTime = LocalTime.of(14, 00);
    private static final LocalTime checkTime = LocalTime.of(20, 00);
    private static final Duration ignoreEditDuration = Duration.ofHours(1).plusMinutes(30);
    private static final String talkChannel = "general";
    private static final String debugChannel = "testare-bot";

    private final Slack slack;
    private final Drive drive;
    private final Discord discord;

    private LocalDateTime createTime = LocalDateTime.MIN;

    public MinuteJob(Slack slack, Drive drive, Discord discord) {
        this.slack = slack;
        this.drive = drive;
        this.discord = discord;
    }

    @Override
    public String getVersionId() {
        return "b3d688b8-5ff8-4ae8-84fa-1141f4f8a0e1";
    }

    @Override
    protected boolean compatibleWithVersionId(String versionId) {
        return getVersionId().equals(versionId)
            || Stream.of().anyMatch(id -> id.equals(versionId));
    }

    @Override
    public String getName() {
        return "WeeklyMinuteWatcher";
    }

    @Override
    public void init() {
        LocalDateTime firstMeeting = getNextRegularMeetingTime(LocalDateTime.now());
        super.registerTimer(firstMeeting, "create_minute");
        logger.info("Initialized a new MinuteJob");
        super.registerTimer(LocalDateTime.now().plusSeconds(3), "periodic");
    }

    @Override
    public void onTimer(String id, LocalDateTime scheduled) {
        if ("create_minute".equals(id)) {
            if (createMinuteAndAnnounce(scheduled)) {
                createTime = scheduled;
                super.registerTimer(getMinuteCheckTime(createTime), "check_minute");
                super.registerTimer(getNextRegularMeetingTime(createTime), id);
            }
        }
        if ("check_minute".equals(id)) {
            checkMinute(scheduled);
        }
        if ("periodic".equals(id)) {
            logger.info("periodic fired");
            super.registerTimer(scheduled.plusSeconds(3), "periodic");
        }
    }

    @Override
    public void deserialize(JSONObject instance) {
        super.deserialize(instance);
        if (instance.has("createTime")) {
            createTime = LocalDateTime.parse(instance.getString("createTime"));
        }
        logger.info("Initialized a saved MinuteJob");
    }

    @Override
    public JSONObject serialize() {
        return super.serialize()
            .put("createTime", createTime.toString());
    }

    private LocalDateTime getNextRegularMeetingTime(LocalDateTime now) {
        LocalDateTime todayMeetingTime = LocalDateTime.of(now.toLocalDate(), meetingTime);
        if (todayMeetingTime.compareTo(now) > 0 && todayMeetingTime.getDayOfWeek().equals(meetingDay)) {
            return todayMeetingTime;
        }
        do {
            todayMeetingTime = todayMeetingTime.plusDays(1);
        } while (!todayMeetingTime.getDayOfWeek().equals(meetingDay));
        return todayMeetingTime;
    }

    private LocalDateTime getMinuteCheckTime(LocalDateTime create) {
        LocalDate date = create.toLocalDate();
        return LocalDateTime.of(date, checkTime);
    }

    private String getMinuteName(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    } 

    private boolean createMinuteAndAnnounce(LocalDateTime createMoment) {
        Optional<File> optMinutes = drive.listFolder("root").stream().filter(f -> f.getName().equals("Minutes")).findAny();
        if (!optMinutes.isPresent()) {
            logger.error("Minutes folder not found");
            slack.postMessage(debugChannel, "ERROR: Minutes folder not found");    
            return false;
        }

        Optional<File> optModel = drive.listFolder(optMinutes.get().getId()).stream().filter(f -> f.getName().equals("Model")).findAny();
        if (!optModel.isPresent()) {
            logger.error("Minute model not found");
            slack.postMessage(debugChannel, "ERROR: Minute model not found");
            return false;
        }

        String minuteName = getMinuteName(createMoment.toLocalDate());

        File copy = drive.copyFile(optModel.get().getId(), minuteName);
        if (copy == null) {
            logger.error("Cannot create copy of minute model");
            slack.postMessage(debugChannel, "ERROR: Cannot create copy of minute model");
            return false;
        }

        String link = drive.getViewLink(copy.getId(), true);
        if (link == null) {
            logger.warn("Cannot create shareable link");
            slack.postMessage(debugChannel, "WARN: Cannot create shareable link");
        }

        String nameMarkup = (link == null ? "minute" : "<" + link + "|" + "minute" + ">");
        String message = "I created a new " + nameMarkup + " \'" + minuteName + "\' in folder Minutes on Google Drive.\nIn the meantime, here's a random fortune:\n\n```";
        message = message + Utils.generateFortune() + "```";
        slack.postMessage(talkChannel, message);
        logger.info("Minute {} created", minuteName);
        return true;
    }

    private boolean checkMinute(LocalDateTime checkMoment) {
        Optional<File> optMinutes = drive.listFolder("root").stream().filter(f -> f.getName().equals("Minutes")).findAny();
        if (!optMinutes.isPresent()) {
            logger.error("Minutes folder not found");
            slack.postMessage(debugChannel, "ERROR: Minutes folder not found");
            return false;
        }

        String minuteName = getMinuteName(createTime.toLocalDate());

        Optional<File> optMinute = drive.listFolder(optMinutes.get().getId()).stream().filter(f -> f.getName().equals(minuteName)).findAny();
        if (!optMinute.isPresent()) {
            logger.warn("Could not find today's minute");
            slack.postMessage(talkChannel, "Seems like I can't reach today's minute. Was it moved or deleted?");
            return true;
        }

        String link = drive.getViewLink(optMinute.get().getId(), false);
        if (link == null) {
            logger.warn("Cannot create shareable link");
            slack.postMessage(debugChannel, "WARN: Cannot create shareable link");
        }

        LocalDateTime lastModified = LocalDateTime.ofInstant(Instant.parse(optMinute.get().getModifiedTime().toStringRfc3339()), ZoneId.systemDefault());

        LocalDateTime ignoreTreshold = createTime.plus(ignoreEditDuration.toMillisPart(), ChronoUnit.MILLIS);

        if (lastModified.compareTo(ignoreTreshold) < 0) {
            logger.info("Detected unedited minute");
            String message = "Looks like <" + link + "|today's minute> wasn't edited. Was the meeting cancelled?";
            slack.postMessage(talkChannel, message);
        } else {
            logger.info("Detected edited minute");
        }
        return true;
    }
}