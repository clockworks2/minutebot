package com.clockworks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.slack.api.bolt.App;
import com.slack.api.bolt.jetty.SlackAppServer;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;

import org.json.JSONObject;
import org.json.JSONTokener;

public class Slack {
    private ExecutorService threadPoolExecutor = Executors.newSingleThreadExecutor();
    private App app;
    private SlackAppServer server;
    private String token;
    private com.slack.api.Slack slack = com.slack.api.Slack.getInstance();

    public Slack() {
        CompletableFuture.runAsync(() -> {
            app = new App();
            try (var auth = SlackAuth.deserialize(".data/slackauth.json")) {
                app.config().setClientSecret(auth.secret);
                app.config().setSigningSecret(auth.signing);
                app.config().setSingleTeamBotToken(auth.token);
                token = auth.token;
            } catch (Exception e) {
                throw new RuntimeException("Cannot authenticate on slack", e);
            }
            app.command("/hello", (r, c) -> c.ack(":wave: Hello!"));
            server = new SlackAppServer(app, 3333);
            new Thread(() -> {
                try {
                    server.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }, threadPoolExecutor);
    }

    public CompletableFuture<Void> postMessage(String channel, String message) {
        return CompletableFuture.runAsync(() -> {
            try {
                ChatPostMessageResponse resp = slack.methods(token)
                        .chatPostMessage(req -> req.channel(channel).text(message));
                String tryAgainChannel;
                if (resp.getError() != null) {
                    if (channel.startsWith("#"))
                        tryAgainChannel = channel.substring(1);
                    else
                        tryAgainChannel = channel + "#";
                    resp = slack.methods(token).chatPostMessage(req -> req.channel(tryAgainChannel).text(message));
                    if (resp.getError() != null)
                        throw new Exception(resp.getError());
                }

            } catch (Exception e) {
                throw new RuntimeException("Failed to send message", e);
            }
        }, threadPoolExecutor);
    }

    public CompletableFuture<Void> close() {
        return CompletableFuture.runAsync(() -> {
            try {
                server.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, threadPoolExecutor);
    }
    



    private static class SlackAuth implements AutoCloseable {
        public final String secret;
        public final String token;
        public final String signing;

        private SlackAuth(String secret, String token, String signing) {
            this.secret = secret;
            this.token = token;
            this.signing = signing;
        }

        private static SlackAuth deserialize(String fileName) throws IOException {
            JSONTokener tokener = new JSONTokener(Files.newInputStream(Paths.get(fileName)));
            JSONObject obj = new JSONObject(tokener);
            return new SlackAuth(obj.getString("secret"), obj.getString("token"), obj.getString("signing"));
        }

        @Override
        public void close() throws Exception {
            // Nothing to close 
        }
    }
}