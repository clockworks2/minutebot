package com.clockworks;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import com.clockworks.job.MinuteJob;
import com.clockworks.schedule.JobScheduler;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static final String pidLocation = "/tmp/MinuteBotPID";
    private static final String jobLocation = ".data/jobs.json";
    private static final Main mainInstance = new Main();

    Slack slack;
    Drive drive;
    Discord discord;
    JobScheduler scheduler;

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> mainInstance.onShutdown()));
        mainInstance.killOtherInstances();
        mainInstance.saveOnlyInstance();
        mainInstance.run(args);
    }

    private void run(String[] args) {
        slack = new Slack();
        drive = new Drive();
        discord = new Discord();
        scheduler = new JobScheduler();
        scheduler.initializeAsSavedInstance(readSavedJobs());
        scheduler.addMultijob(new MinuteJob(slack, drive, discord));
        scheduler.start();
    }

    private void onShutdown() {
        logger.info("Exiting gracefully");
        if (slack != null) {
            try {
                logger.info("Closing slack");
                slack.close().get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                logger.warn("Failed to close slack", e);
            }
        }
        if (scheduler != null) {
            String json = scheduler.stop();
            try {
                Files.write(Paths.get(jobLocation), json.getBytes());
            } catch (IOException ioex) {
                logger.error("Cannot write jobs to disk", ioex);
            }
        }
        logger.info("Exiting done");
    }

    private String readSavedJobs() {
        try {
            return Files.readString(Paths.get(jobLocation));
        } catch (IOException ioex) {
            logger.warn("Cannot read jobs from disk");
        }
        return new JSONArray().toString();
    }

    private void killOtherInstances() {
        String pids = "";
        try {
            pids = Files.readString(Paths.get(pidLocation));
        }  catch (NoSuchFileException unused) {
            logger.info("No pid file found");
        } catch (IOException ioex) {
            logger.warn("Failed to read pid file", ioex);
        }
        Arrays.stream(pids.split("\n")).filter(pid -> pid.matches("[0-9]+")).forEach(pid -> {
            try {
                logger.info("Sending SIGTERM to pid {}, waiting for it to exit", pid);
                Runtime.getRuntime().exec(String.format("kill -s TERM %s", pid));
                ProcessHandle.of(Long.parseLong(pid)).ifPresent(ph -> {
                    try {
                        ph.onExit().get();
                        logger.info("Wait finished");
                    } catch (InterruptedException | ExecutionException e) {
                        logger.warn("Wait interrupted", e);
                    }
                });
            } catch (IOException e) {
                logger.warn("Could not kill pid " + pid, e);
            }
        });
    }

    private void saveOnlyInstance() {
        try {
            OutputStream outwriter = Files.newOutputStream(Paths.get(pidLocation), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
            long pid = ProcessHandle.current().pid();
            outwriter.write(Long.toString(pid).getBytes());
            outwriter.write('\n');
            outwriter.close();
        } catch (IOException ioex) {
            logger.error("Cannot save instance pid", ioex);
            System.exit(-1);
        }
    }

}